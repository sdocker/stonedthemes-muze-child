<?php
/*
Template Name: Contact
*/
?>
<?php

$formSubmitted = false;
$sentMessage = false;


if(get_field("disable_ajax","options")){
    if(isset($_POST["submitted"]) && $_POST["submitted"]){
        if(!(isset($_POST["full_name"]) && $_POST["full_name"]))
        {
            throwError('Full Name');
        }
        elseif(!(isset($_POST["email"]) && $_POST["email"])){
            throwError('Email');
        }
        elseif(!(isset($_POST["comment"]) && $_POST["comment"])){
            throwError('Comment');
        }
        else
        {

            $name = $_POST["full_name"];
            $email = $_POST["email"];
            $message = $_POST["comment"];
			 $subject = $_POST["subject"];
            $to_email = get_field('email_receiver');
            if(!$to_email){
                $to_email = get_option('admin_email');
            }
               
				$message = "
				Name : {$name},
				Email : {$email}

				$message
                            ";
                if(wp_mail($to_email,$subject,$message))
                {
                    $sentMessage = true;
                }
                else{
                    $sentMessage = false;
                }
            $formSubmitted = true;
        }
    }
}
if(isset($_REQUEST["sth_name"]) && isset($_REQUEST["sth_email"]) && isset($_REQUEST["sth_subject"]) && isset($_REQUEST["sth_message"])){
    $name = $_REQUEST["sth_name"];
    $email = $_REQUEST["sth_email"];
    $message = $_REQUEST["sth_message"];
	$subject = $_REQUEST["sth_subject"];    
			
	$to_email = get_option('admin_email');
		
	 
		$message = "
			Name : {$name},
			Email : {$email}
			
			$message
		";
	  try{
		if(wp_mail($to_email,$subject,$message))
		{
			echo  'Your message was sent successfully. We look forward to talking with you soon!';
			
		}
		else{
			echo 'Message sending failed!';
		}
		}
		catch(Exception $e){ echo "Error ".$e;}
	die();
}

get_header();
?>

<!-- removed Google map javascript -->

	<?php
    	$latLng = get_field('map');
    	$zoomLevel = get_field('map_zoom');		
	?>

  <div id="title-container" class="full-background-image" style="background: url('<?php the_field("header_background");?>');">
	<div id="linearBg" class="title-container-pattern" style="<?php header_style(); ?>"></div>
	<div class="title-info">
		<h1 class="light"><?php the_title();?></h1>
		<h4 class="light"><?php the_field("header_subtitle");?></h4>
	</div>
   </div>
	

	<!-- <div> -->
<div class="container">
<!-- START: Yoast breadcrumb -->
<?php
if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('
<p id="breadcrumbs">','</p>
');
}
?>
<!-- END: Yoast breadcrumb -->
			<div class="col-md-6">
				<h3 class="normal no-margin"><i class="fa fa-envelope-o before-heading"></i><?php the_field('contact_form_title'); ?></h3>
				<div class="row contact-container">
				 <?php if(!$formSubmitted):?>
					<form class="contactForm" action="" method="post" >
							<input type="hidden" name="submitted" value="true" />							
							<div class="col-md-6">
								<div class="input-wrapper fa-user">
									<input type="text" name='full_name' id='full_name' placeholder="<?php the_field('full_name_field'); ?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-wrapper fa-envelope">
									<input type="text" name='email' id='email' placeholder="<?php the_field('email_field'); ?>">
								</div>
							</div>	
							<div class="col-md-12">
								<div class="input-wrapper fa-mail-reply">
									<input type="text" name='subject' id='subject' placeholder="<?php the_field('subject_field'); ?>">
								</div>
							</div>	
							<div class="col-md-12">
								<textarea name='comment' id='comment'  placeholder="<?php the_field('message_field'); ?>"></textarea>
								<div class="corner"></div>
							</div>	
							<div class="col-md-12">
								<button type="submit" class="btn1-muze btn-block show"><?php _e('send message','sth_hybrid');?></button>
							</div>					
					</form>
					<?php endif; ?>
						<div>
                            <h1 class="sth_message"></h1>
                        </div>
					  <?php if($formSubmitted): ?>
                        <div>
                            <?php if($message == true): ?>
                                <h1><?php _e('Your message was sent successfully!','sth_hybrid');?></h1>
                            <?php else: ?>
                                <h1><?php _e('Message sending failed!','sth_hybrid');?></h1>
                            <?php endif;?>
                        </div>
                    <?php endif;?>
				</div>
			</div>
			<div class="col-md-6">
	
		<div class="contact-info">
			<i class="fa fa-asterisk fa-2x"></i>
			<h3><?php the_field('title'); ?></h3>
			<?php the_field('description'); ?>
		</div>

	
	
		<div class="contact-info">
			<i class="fa fa-location-arrow fa-2x"></i>
			<h3><?php the_field('address_title'); ?></h3>
			<?php the_field('address_info'); ?>
		</div>

	</div>
		</div>
	<!-- </div> -->
	
<?php get_footer();?>