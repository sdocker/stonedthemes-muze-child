<?php if(!isset($_REQUEST["sth_ajax_load_site"])): ?>  
</div><!-- ./Main Container -->
<footer>
<div class="container widget-area">
<div class="row">
</div><!-- ./col-md-4 -->
<?php if ( is_active_sidebar( 'sidebar-2' )  ) : ?><div class="col-md-4"><?php dynamic_sidebar( 'sidebar-2' ); ?></div><?php endif; ?>
<?php if ( is_active_sidebar( 'sidebar-3' )  ) : ?><div class="col-md-4"><?php dynamic_sidebar( 'sidebar-3' ); ?></div><?php endif; ?>
<div class="col-md-4">
<div class="widget">
<?php the_field('left_content','options'); ?>
<div class="footer-social">
<?php $social_medias =  get_field('social_networks','options'); ?>
<?php if($social_medias): ?>
<div class="header-social-container">
<div class="header-social">
<?php foreach($social_medias as $social_media):?>
<div class="social">
<a href="<?php echo $social_media['url'];?>"><i class="fa <?php echo $social_media['network']; ?>"></i></a>
</div><!-- ./social -->
<?php endforeach;?>
</div><!-- header-social-->
</div><!-- header-social-container -->
<?php endif;?> 
</div><!-- ./footer-social -->
</div><!-- ./widget text-left -->
</div><!--./row -->
</div><!-- ./container widget-area -->
</footer>
<!-- </div> -->
<div class="csspinner double-up">
</div>
<?php wp_footer(); ?>
</body>
</html>
<?php endif;?>