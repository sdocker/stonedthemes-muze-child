<?php
/*
Template Name: Partners
*/
?>
<?php get_header();?>
<div id="title-container" class="full-background-image" style="background: url('<?php the_field("header_background");?>');">
<div id="linearBg" class="title-container-pattern" style="<?php header_style(); ?>"></div>
<div class="title-info">
<h1 class="light"><?php the_title();?></h1>
<h4 class="light"><?php the_field("header_subtitle");?></h4>
</div>
</div>
<div id="partenrs-container">
<div class="container">
<!-- START: Yoast breadcrumb -->
<?php
if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('
<p id="breadcrumbs">','</p>
');
}
?>
<!-- END: Yoast breadcrumb -->
<div class="row">
<?php $partners =  get_field('partners');?>
<?php if($partners): ?>
<?php foreach($partners as $partner):?>
<div class="col-md-4 col-sm-6">
<div class="partners-item">
<div class="pattern-container curl-bottom-right">
<div class="pattern opaque">
<div class="center-vertical-container">
<div class="center-vertical-content">
<?php $image = wp_get_attachment_image_src( $partner['image'] , 'partners'); ?>
<img src="<?php echo $image[0]; ?>" alt="partner-item">
</div>
</div>
</div>
<div class="pattern">
<div class="pattern pattern-white partner-item-info">
<div>
<a href="<?php echo $partner['link']; ?>"><h4 class="text-center text-orange"><?php echo $partner['partner_name']; ?></h4></a>
<p class="text-center"><?php echo $partner['partner_info']; ?></p>
</div>
</div>
</div>
</div>
 </div><!-- ./partners-item -->
</div> 
<?php endforeach;?>
<?php endif;?>   
</div>
</div>
</div>
<?php get_footer();?>