<?php get_header(); ?>
<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),'full'); ?>
<div id="title-container" class="full-background-image" style="background: url('<?php echo $large_image_url[0];?>');">
<div class="title-container-pattern portfolio-pattern opaque8"></div>
<div class="title-info">
<h1 class="light"><?php the_title();?></h1>			
</div>
</div><!-- ./title-container -->

<div class="single-blog">
<div class="container">
<div class="single-blog-content-container">
<!-- START: Yoast breadcrumb -->
<?php
if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('
<p id="breadcrumbs">','</p>
');
}
?>
<!-- END: Yoast breadcrumb -->
				<div class="col-md-8">
					<div class="blog-img">
						<?php 	
							if ( get_post_format( $post->ID ) =='' ) {?>							
								<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
								<img src="<?php echo $url; ?>" class="img-responsive" alt="blog-img">						
							<?php } elseif (has_post_format( 'video' ) ) {?>
								<?php if(get_field('video_types') == "youtube"):?>
									 <iframe width="650" height="500" src="https://www.youtube.com/embed/<?php the_field('video_url');?>" allowfullscreen></iframe>
								<?php elseif(get_field('video_types') == "vimeo"):?>
									 <iframe src="//player.vimeo.com/video/<?php the_field('video_url');?>" width="650" height="500" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<?php else:?>
									 <?php if(get_field('selfhosted_file')!=''){ ?><video width="650" height="500" controls><source src="<?php the_field('selfhosted_file'); ?>" type="video/mp4"><?php _e('Your browser does not support the video tag.','sth_lang');?></video><?php } ?>
								<?php endif;?>
							<?php } elseif (has_post_format( 'audio' ) ) {
									if(get_field('audio_type')== "url"){
										$media = get_field('url');
									}
									else{
										$media = get_field('audio_file');
									}
								?>
								<?php if($media!=''): ?><audio id="audioPlayer" src="<?php echo $media;?>" type="audio/mp3" controls="controls"><?php endif; ?>
							<?php } elseif (has_post_format( 'gallery' ) ) {?>
								<?php $gallery_images = get_field('gallery_images'); ?>
								<?php if($gallery_images):?>
									<div id="blog-carousel" class="carousel slide" data-ride="carousel">
										<div class="carousel-inner">
										<?php $gallery_counter = 1;?>
										<?php foreach($gallery_images as $gallery_item) : ?>
											<div class="item <?php if($gallery_counter==1){ echo 'active'; } ?>">
											<?php $image = wp_get_attachment_image_src(  $gallery_item['image'], 'portfolio' ); ?>
											<img src="<?php echo $image[0];?>" alt="">
											</div>
										<?php $gallery_counter++; endforeach; ?>										
										</div>
									  	<a class="lightbox-carousel-nav prev" href="#blog-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
					 					<a class="lightbox-carousel-nav next" href="#blog-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
									</div>
								<?php endif; ?>
							<?php } ?>
					</div><!-- blog-img -->
					<div class="single-blog-content">
						<div class="single-blog-title">
						<?php
							$post_categories  = get_the_category();
							$post_categories_names = '';
							$post_categories_realNames = '';
							if($post_categories){
								foreach ($post_categories as $post_cat){
									$post_categories_names .= str_replace(' ', '_',$post_cat->name)." ";
									$post_categories_realNames .= $post_cat->name.' ';
								}
							}
							?>
							<p class="smaller-p text-uppercase text-white"><?php echo $post_categories_names;?></p>
	                    	<h4 class="light"><?php the_title();?></h4>
						</div>
                    	<?php  setup_postdata($post);?>
						<?php  the_content();?>
						<?php  wp_link_pages();?>	
						<?php  wp_reset_postdata();?>
					</div>
					<?php if(!get_field('disable_blog_social_shares','options')):?>		  
						<div class="single-blog-shares-container">
							<div class="single-blog-shares pull-right">
								<?php addSocialShareButtons(get_field('share_projects','options'));?>								
							</div>
							<span><?php _e('Enjoyed the artcile?','sth_lang');?></span>
						</div>
					 <?php endif ; ?>
					  <div id="tags">
						<p><?php _e("Tags:", "sth_lang");?></p>
								<p>
								<?php if(get_the_tags()):?>		
											<?php
												$tags = get_the_tags();
												$c = sizeof($tags);											
												foreach ( $tags as $tag ) {
														$tag_link = get_tag_link( $tag->term_id );															
														$html = "<a href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug}'>";
														$html .= "{$tag->name}</a> ";
														$c--;
														if($c!=0)
															$html .= "";
														echo $html;													
													}
											?>										
										<?php endif;?>
								</p>
					  </div>
        			<?php comments_template(); ?>	
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>></div>
				</div><!-- ./col-md-8 -->
			</div><!-- ./single-blog-content-container -->
			<?php if(is_active_sidebar('sidebar-1')):?>
				<div class="col-md-4">
					<?php  if ( !function_exists('dynamic_sidebar') ||  !dynamic_sidebar('sidebar-1') ) ?>
				</div>
				<?php endif;?>
		</div><!-- ./container -->
	</div><!-- single-blog -->
	<script>
	  jQuery(document).ready(function($){
	    jQuery('.carousel').carousel();
		shortcodes();
	  });
	</script>
<?php get_footer();?>