<?php
/*
Template Name: Portfolio1
*/
/*Project Loop*/
	if(isset($_REQUEST["sth_page"])):    	
 			if(isset($_POST["paged"])):
			 $paged = $_POST["paged"];
			  $args = array(
						'post_type' => 'project_post',
						'paged'     => $paged
					);					   
				$the_Query = new WP_Query($args);
			
				while ($the_Query->have_posts()):
					$the_Query->the_post(); 							
				?>
				<?php
					global $post;
					setup_postdata($post);
					$terms = get_the_terms( $post->ID, 'projects_category' );
					$post_terms = '';
					$post_term_name = "";
					if($terms){
						foreach ($terms as $term){
							$post_terms .= str_replace(' ', '_', $term->name).' ';
							$post_term_name .= $term->name.' ';
						}
					}
				?>
				 <div class="portfolio-item element <?php echo $post_terms;?> col-md-4 col-xs-12">
             <div class="curl-bottom-right">
						    <div class="portfolio-item-img">
	                           <?php the_post_thumbnail( 'portfolio');?>
	                        </div>
	                        <div class="portfolio-pattern"></div>    
	                        <div class="no-padding portfolio-item-info">
	                            <p class="smaller-p"><a class="st_ajaxLink" href="<?php the_permalink(); ?>"><?php echo $post_term_name; ?></a></p>
	                            <a class="st_ajaxLink" href="<?php the_permalink(); ?>">
									<h4 class="light"><?php the_title(); ?>xxxx</h4>
								 </a>
	               </div>
                    	</div>
                  </div>
				<?php endwhile; ?> 
			<?php endif;
	die();endif; 
?>
<?php get_header();?>
<div id="title-container" class="full-background-image" style="background: url('<?php the_field("header_background");?>');">
	<div id="linearBg" class="title-container-pattern" style="<?php header_style(); ?>"></div>
	<div class="title-info">
		<h1 class="light"><?php the_title();?></h1>
		<h4 class="light"><?php the_field("header_subtitle");?></h4>
	</div>
</div>
<div class="row">
<div class="container">
<!-- START: Yoast breadcrumb -->
<?php
if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('
<p id="breadcrumbs">','</p>
');
}
?>
<!-- END: Yoast breadcrumb -->
<div class="row">
<div class="col-md-12">
<ul id="filters" class="blog-filters">
<li id="filter-caption"><a href=""><?php _e('Filters','sth_lang');?></a></li>
<li><a href="" data-filter="*"><?php _e('all categories','sth_lang');?></a></li>
	<?php
		$categories =  get_terms('projects_category');
		foreach($categories as $cat):?>
			<li><a href="" data-filter=".<?php echo str_replace(' ','_',$cat->name);?>"><?php echo str_replace(' ','&nbsp;',$cat->name);?></a></li>
		<?php endforeach;?>
</ul>
</div><!-- ./col-md-12 -->
</div><!-- ./row -->
<div id="portfolio-container" class="row">
					<?php 
					   $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					   $args = array(
							'post_type' => 'project_post',
							'paged'     => $paged
						);					   
						$the_Query = new WP_Query($args);
						 
						while ($the_Query->have_posts()):
							$the_Query->the_post(); 							
					?>
					<?php
						global $post;
						setup_postdata($post);
						$terms = get_the_terms( $post->ID, 'projects_category' );
						$post_terms = '';
						$post_term_name = "";
						if($terms){
							foreach ($terms as $term){
								$post_terms .= str_replace(' ', '_', $term->name).' ';
								$post_term_name .= $term->name.' ';
							}
						}
					?>
<div class="partners-item element <?php echo $post_terms;?> col-md-4 col-sm-6 col-xs-12">
<div class="pattern-container curl-bottom-right">
<div class="pattern opaque">
<div class="center-vertical-container">
<div class="center-vertical-content">
<div class="portfolio-item-img">
<?php the_post_thumbnail( 'portfolio');?>
</div>
</div>
</div>
</div><!-- ./portfolio-item-img -->
<div class="pattern">
<div class="portfolio pattern pattern-white partner-item-info">
<div>
<p class="smaller-p"><a class="st_ajaxLink" href="<?php the_permalink(); ?>"><?php echo $post_term_name; ?></a></p>
<h4 class="light"><a class="st_ajaxLink" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
<p class="summary"><?php  the_content();?></p>
</div>
</div>
</div>
</div>
</div><!-- ./partners-item element -->
<?php endwhile; ?> 
</div>


				<?php if($the_Query->max_num_pages != 1):?>
					<div class="text-center">
						<a href="" class="portfolio-load  light loadMoreBtn">
							<p><?php _e('load more','sth_lang');?></p>
							<!-- <div class="csspinner spinner-load-more double-up no-overlay">
							</div> -->
							<div class="dots">
								<div class="current"></div>
								<div></div>
								<div></div>
							</div>
						</a>
					</div>
				<?php endif;?>              
            </div>
        <!-- </div> -->
    </div>
	<script>
	var page = parseInt("<?php echo $paged; ?>");
    var last_page = parseInt('<?php echo $the_Query->max_num_pages; ?>');
	
	jQuery(document).ready(function($) {
		portfolio1();
	});
	</script>
<?php get_footer();?>