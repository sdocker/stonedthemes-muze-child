<?php if(!isset($_REQUEST["sth_ajax_load_site"])): ?>
<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />	

<?php if(get_field("fav_icon","options")): ?>
<link rel="icon" type="image/png" href="<?php the_field("fav_icon","options"); ?>" />
<?php endif; ?>
<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
<link href="https://fonts.googleapis.com/css?family=Bree+Serif|Lato" rel="stylesheet">
<script>
var template_directory = "<?php echo get_template_directory_uri(); ?>";
var ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";
var postId = "<?php if(isset($post->ID)) {echo $post->ID; }?>";	
var ajaxDisabled = "<?php echo get_field("disable_ajax","options");?>";		
</script>  		

<?php wp_head(); ?>
		
<style>
<?php 	set_logo_dimensioins();	?>
</style>

<?php
		if(get_field('custom_css','options'))
		{
		  the_field('custom_css','options');
		}
		if(get_field('custom_javascript','options'))
		{
			the_field('custom_javascript','options');
		}  
?>
<?php
		if(get_field('brand_color','options')!=''){
				brand_color(  get_field('brand_color','options'));
		}
		if(get_field('gradient_color1','options')!=''&&get_field('gradient_color2','options')!=''){
				brand_overlay(get_field('gradient_color1','options'),get_field('gradient_color2','options'),get_field('gradient_opacity','options'));
		}
		// register_custom_fonts();
?>

<link rel='stylesheet' id='custom-css'  href='/wp-content/themes/StonedThemes-Muze-child/custom.css' type='text/css' media='all' />
</head>

<body <?php body_class(); ?>>
<!-- <div id="gray" class="load-bck active"> -->
<div class="full-background-image fixed-pattern" style="background:url('<?php the_field('background_image','options'); ?>')">
<div class="fixed-pattern" style="background: rgba(<?php $hex = get_field("background_color","options");list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");echo "$r".","."$g".","."$b".",".(get_field("background_opacity","options")/10).");";?>"></div>
</div>

<header>
<div class="container">
<div class="center-logo">			        	
<?php if(get_field("logo_type","options")=="image"){ ?>
<?php $logo = get_field('logo','options');?>
<?php $image = wp_get_attachment_image_src($logo,'full'); ?>
<a href="<?php echo get_site_url(); ?>" class="logo">
<img src="<?php echo $image[0];?>" alt="logo">
</a>
<?php } else if (get_field("logo_type","options")=="text"){ ?>
<?php $font = get_field('text_logo_font','options');?>
<a href="<?php echo get_site_url(); ?>"  class="logo" style="width: auto; color:<?php the_field('text_logo_color','options'); ?>!important; font-family: <?php echo $font['family']; ?>; font-size: <?php the_field('text_font_size','options'); ?>px; " class="titleLogo"><?php the_field('text_logo','options');?></a>
<?php } else { ?>
<a href="<?php echo get_site_url(); ?>" class="logo"><?php bloginfo('name');?></a>
<?php } ?>
</div>
<p class="responsive-navigation bold"><i class="fa fa-bars pull-right"></i><?php $social_medias =  get_field('social_networks','options'); ?>
<?php if($social_medias): ?>
<span class="header-social">
<?php foreach($social_medias as $social_media):?>
<span class="social">
<a href="<?php echo $social_media['url'];?>"><i class="fa <?php echo $social_media['network']; ?>"></i></a>
</span>
<?php endforeach;?>
</span>
<?php endif;?> </p>
			
<!--menu starts here-->
<div class="main-menu">
<?php wp_nav_menu(array( 'theme_location' => 'primary', 'container_class' => 'menu-menu-container'));?>
</div><!--menu ends here-->

<?php $social_medias =  get_field('social_networks','options'); ?>
<?php if($social_medias): ?>
<div class="header-social-container pull-right hidden-xs">
<div class="header-social">
<?php foreach($social_medias as $social_media):?>
<div class="social">
<a href="<?php echo $social_media['url'];?>"><i class="fa <?php echo $social_media['network']; ?>"></i></a>
</div>
<?php endforeach;?>
</div>
</div>
<?php endif;?> 
      		
</div>
</header>	
<div class="main_container">
<?php endif; ?><!-- st_ajax_load_site-->			