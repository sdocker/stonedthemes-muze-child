<?php
/*
Template Name:Blog2
*/
$postspage_id = 0;
if(!is_page()){
    $postspage_id = get_option('page_for_posts');
    $post = get_post($postspage_id);

}
/*Blog Loop*/
 	if(isset($_REQUEST["sth_page"])):    	
 			if(isset($_POST["paged"])):
			
			   $paged = $_POST["paged"];
			   $args = array(
					'post_type' => 'post',
					'paged'     => $paged
				);
			   
				$the_Query = new WP_Query($args);				
				while ($the_Query->have_posts()):
					$the_Query->the_post(); 							
			
				$post_categories  = get_the_category();
				$post_categories_names = '';
				$post_categories_realNames = '';
				if($post_categories){
					foreach ($post_categories as $post_cat){
						$post_categories_names .= str_replace(' ', '_',$post_cat->name)." ";
						$post_categories_realNames .= $post_cat->name.' ';
					}
				}
			?>
			<div class="blog2-item">
				<div class="blog2-img">
					<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
					<img class="img-responsive" src="<?php echo $url;?>" alt="blog-item">
				</div>
				<div class="blog2-info">
					<p class="smaller-p text-uppercase"><?php echo $post_categories_names;?></p>
					<a href="<?php echo get_permalink();?>" class="st_ajaxLink"><h4 class="light"><?php the_title(); ?></h4></a>
					<a href="<?php echo get_permalink();?>" class="st_ajaxLink"><div class="short-desc"><?php the_excerpt(); ?></div></a>
				</div>
			</div>
		<?php endwhile; ?>  
	<?php endif;

	die();endif; 	
				
?>
<?php get_header();?>
<div id="title-container" class="full-background-image" style="background: url('<?php the_field("header_background");?>');">
	<div id="linearBg" class="title-container-pattern" style="<?php header_style(); ?>"></div>
	<div class="title-info">
		<h1 class="light"><?php the_title();?></h1>
		<h4 class="light"><?php the_field("header_subtitle");?></h4>
	</div>
</div>
<div class="container">
	<div class="col-md-8">
		<div id="blog2-container">
		
<!-- START: Yoast breadcrumb -->
<?php
if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('
<p id="breadcrumbs">','</p>
');
}
?>
<!-- END: Yoast breadcrumb -->
			<?php 
			   $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			   $args = array(
					'post_type' => 'post',
					'paged'     => $paged
				);
			   
				$the_Query = new WP_Query($args);
				 $counter = 0;
				while ($the_Query->have_posts()):
					$the_Query->the_post(); 							
			?>
			<?php
				$post_categories  = get_the_category();
				$post_categories_names = '';
				$post_categories_realNames = '';
				if($post_categories){
					foreach ($post_categories as $post_cat){
						$post_categories_names .= str_replace(' ', '_',$post_cat->name)." ";
						$post_categories_realNames .= $post_cat->name.' ';
					}
				}
			?>
			<div class="blog2-item">
				<div class="blog2-img">
					<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
					<img class="img-responsive" src="<?php echo $url;?>" alt="blog-item">
				</div>
				<div class="blog2-info">
					<p class="smaller-p text-uppercase"><?php echo $post_categories_names;?></p>
					<a href="<?php echo get_permalink();?>" class="st_ajaxLink"><h4 class="light"><?php the_title(); ?></h4></a>
					<a href="<?php echo get_permalink();?>" class="st_ajaxLink"><div class="short-desc"><?php the_excerpt(); ?></div></a>
				</div>
			</div>
		<?php endwhile; ?>   
       </div>		
		<?php if($the_Query->max_num_pages != 1):?>
			<div class="text-center">
				<a href="" class="portfolio-load  light loadMoreBtn">
					<p><?php _e('load more','sth_lang');?></p>
					<div class="dots">
						<div class="current"></div>
						<div></div>
						<div></div>
					</div>
				</a>
			</div>
		<?php endif;?>
	</div><!-- ./col-md-8 -->
	<?php if(is_active_sidebar('sidebar-1')):?>
		<div class="col-md-4">
			<?php  if ( !function_exists('dynamic_sidebar') ||  !dynamic_sidebar('sidebar-1') ) ?>
		</div><!-- ./col-md-4 -->
	<?php endif;?>
</div>
  <script>
	var page = parseInt("<?php echo $paged; ?>");
    var last_page = parseInt('<?php echo $the_Query->max_num_pages; ?>');
	
	jQuery(document).ready(function($) {
		blog2();
	});
	</script>
<?php get_footer();?>