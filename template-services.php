<?php
/*
Template Name: Services
*/
?>
<?php get_header();?>
<div id="title-container" class="full-background-image" style="background: url('<?php the_field("header_background");?>');">
	<div id="linearBg" class="title-container-pattern" style="<?php header_style(); ?>"></div>
	<div class="title-info">
		<h1 class="light"><?php the_title();?></h1>
		<h4 class="light"><?php the_field("header_subtitle");?></h4>
	</div>
</div>
<div class="container">
<!-- START: Yoast breadcrumb -->
<?php
if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('
<p id="breadcrumbs">','</p>
');
}
?>
<!-- END: Yoast breadcrumb -->
<div class="wrapper">
	<?php
    		$args = array(
    			'post_type' => 'service_post'	,
    			'nopaging' => true,
				'order' => 'ASC'
    		);					   
  		  $the_Query = new WP_Query($args);
  		
  		  while ($the_Query->have_posts()):
  			$the_Query->the_post(); ?>
			  <div class="col-md-4">
				<div class="service service-item fpc_box">
				  <div class="service-photo">
				 <?php if(get_field('background_image',$post->ID))	{								
						$image = wp_get_attachment_image_src( get_field('background_image',$post->ID), 'authors' );
							echo '<img src="'.$image[0].'" alt="service-item" />';
						}						
					  ?>
					<div class="service-photo-pattern" style="background: <?php the_field('font_awesome_color'); ?>;"></div>
					<div class="icon">
						<?php if(get_field('service_type')=='fontawsome'): ?>
							<i class="fa <?php the_field('font_awesome'); ?> fa-3x"></i>
						<?php else : ?>
							<img src="<?php the_field('image'); ?>" />
						<?php endif; ?>
					</div>
					<div class="fpc_corner-box">
						<a class="fpc_page-tip" href="#">
						  <div class="fpc_corner-contents"></div>
						</a>
					</div>
				  </div>
				  <h3><?php the_title();?></h3>
					<?php the_content();?>
				</div>
			  </div>	  
	   <?php endwhile; ?>   	  
    </div>
</div>
<script>
	jQuery(document).ready(function($) {
		services();
	});
</script>
<?php get_footer();?>