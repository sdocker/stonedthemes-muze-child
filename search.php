<?php get_header();?>	
	 
<div id="title-container" class="full-background-image" style="background: url('<?php the_field("header_background");?>');">
	<div id="linearBg" class="title-container-pattern"></div>
	<div class="title-info">
		<?php if($wp_query->have_posts()){?>
			<h1 class="light"><?php _e('Search Results','sth_lang');?></h1>
		<?php } else {?>
		     <h1 class="light"><?php _e('No Result found','sth_lang'); ?></h1>
             <p class="text-center"> <?php _e("Sorry we couldn't find any results.",'sth_lang'); ?></p>
             <p class="smile text-center"><i class="fa fa-meh-o fa-3x"></i></p>
			<?php }?>
	</div>
</div>
<div class="container">
	<div class="col-md-8">
		<div id="blog2-container">
	
			<?php  while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
		
			<?php
				$post_categories  = get_the_category();
				$post_categories_names = '';
				$post_categories_realNames = '';
				if($post_categories){
					foreach ($post_categories as $post_cat){
						$post_categories_names .= str_replace(' ', '_',$post_cat->name)." ";
						$post_categories_realNames .= $post_cat->name.' ';
					}
				}
			?>
			<div class="blog2-item">
				<div class="blog2-img">
					<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
					<img class="img-responsive" src="<?php echo $url;?>" alt="blog-item">
				</div>
				<div class="blog2-info">
					<p class="smaller-p text-uppercase"><?php echo $post_categories_names;?></p>
					<a href="<?php echo get_permalink();?>" class="st_ajaxLink"><h4 class="light"><?php the_title(); ?></h4></a>
					<div class="short-desc"><?php the_excerpt(); ?></div>
				</div>
			</div>
		<?php endwhile; ?> 
		</div>
	<?php if($wp_query->max_num_pages != 1):?>		
			<div class="text-center">
				<nav>
                    <h3 class="assistive-text"><?php _e( 'Post navigation', 'sth_lang' ); ?></h3>                      
					<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">←</span> Older posts', 'sth_lang' ) ); ?></div>
					<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">→</span>', 'sth_lang' ) ); ?></div>
				</nav><!-- #nav-above -->
				<?php wp_link_pages(); ?>
			</div>		
	<?php endif;?>		
	</div>
	<?php if(is_active_sidebar('sidebar-1')):?>
		<div class="col-md-4">
			<?php  if ( !function_exists('dynamic_sidebar') ||  !dynamic_sidebar('sidebar-1') ) ?>
		</div>
	<?php endif;?>
		
</div>
<?php get_footer();?>