<?php
/*
Template Name: About
*/
?>
<?php get_header();?>
<div id="title-container" class="full-background-image" style="background: url('<?php the_field("header_background");?>');">
<div id="linearBg" class="title-container-pattern" style="<?php header_style(); ?>"></div>
<div class="title-info">
<h1 class="light"><?php the_title();?></h1>
<h4 class="light"><?php the_field("header_subtitle");?></h4>
</div>
</div>
	
<!-- Our Team -->
<div id="team" class="wrapper">
<!-- <div class="row"> -->
<div class="container">
<!-- START: Yoast breadcrumb -->
<?php
if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('
<p id="breadcrumbs">','</p>
');
}
?>
<!-- END: Yoast breadcrumb -->
				<div>
					<div>
					<?php $teamMembers =  get_field('team_members'); ?>
					<?php if($teamMembers): ?>
							<?php foreach($teamMembers as $member):?>
								<div class="team-member col-md-6">
									<div class="member-image-container">
										<?php if($member['image'])	{								
											$image = wp_get_attachment_image_src( $member['image'], 'authors' );
												echo '<img src="'.$image[0].'" class="img-responsive" alt="member-img"/>';
											}						
										  ?>
										<div class="team-pattern"></div>
										<div class="member-socials">
										 <?php $socialNetworks = $member['social_networks'];?>							
										 <?php if($socialNetworks): ?>
											<?php foreach($socialNetworks as $network):?>
												<div class="social">
													<a href="<?php echo $network['url'];?>"><i class="fa fa-<?php echo $network['network']; ?>"></i></a>
												</div>
											<?php  endforeach; ?>
										<?php endif;?>	
										</div>
									</div>
									<h3 class="text-center"><a href="<?php echo $member['link'];?>"><?php echo $member['name'];?></a></h3>
									<p class="text-center position"><?php echo $member['postion']; ?></p>
									<div class="text-center"><p><?php echo $member['informations'];?></p></div>
								</div>
							<?php endforeach;?>	
					<?php endif;?>
						
					</div>
				</div>
			</div>
		<!-- </div> -->
	</div>
	<!-- End of Our Team -->
	
	<!-- Our Focus -->
	<div id="focus" class="wrapper">
		<!-- <div class="row"> -->
			<div class="container">
				<h3 class="text-center"><?php the_field("our_focus_title");?></h3>
				<div class="panel-group" id="accordion">
				<?php $focusitems =  get_field('our_focus_list'); $focus_counter=0;?>
					<?php if($focusitems): ?>
							<?php foreach($focusitems as $focus):?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h5 class="panel-title light">
											<a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $focus_counter;?>" <?php if($focus_counter>0) {echo 'class="collapsed"';}?>>
											  <?php echo $focus['focus_title'];?>
											</a>
										</h5>
									</div>
									<div id="<?php echo $focus_counter;?>" class="panel-collapse collapse <?php if($focus_counter==0) {echo 'in';}?>">
										<div class="panel-body">
											<?php echo $focus['focus_informations']; ?>
										</div>
									</div>
								</div>
						<?php $focus_counter++; endforeach;?>	
					<?php endif;?>
				</div>
			</div>
		<!-- </div> -->
	</div>
	<!-- End of Our Focus -->
	
	<!-- Testimonial section removed for launch. See parent theme -->
	
	
    <!-- End of testimonial -->
	
	<!-- Fun Fact -->
	<div id="fun-facts" class="wrapper">
		<!-- <div class="row"> -->
			<div class="container">
				<h3 class="text-center"><?php the_field("fun_facts_title");?></h3>
				<div class="facts">
				<?php $funfacts =  get_field('fun_facts_items');?>
					<?php if($funfacts): ?>
							<?php foreach($funfacts as $funfact):?>
								<div class="fact col-sm-6 col-md-3">
									<div class="fact-number">
										<p><?php echo $funfact['number'];?></p>
									</div>
									<div class="fact-info">
										<p class="normal"><?php echo $funfact['title'];?></p>
										<h4 class="light"><?php echo $funfact['info'];?></h4>
									</div>
								</div>
							<?php endforeach;?>	
					<?php endif;?>				
				</div>
			</div>
		<!-- </div> -->
	</div>
	<!-- End of fun fact -->
	 
	<script>
	jQuery(document).ready(function($) {
		about();
	});
	</script>
<?php get_footer();?>