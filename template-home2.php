<?php
/*
Template Name: Home2
*/
?>
<?php get_header();?>

<!-- start: template-home2.php -->
<div class="fixed-pattern" <?php if(get_field('pattern')!='') { ?> style="background: url('<?php echo get_field('pattern'); ?>');" <?php } ?> id="linearBg1">
</div><!-- ./fixed-pattern -->
<div id="slides">
<?php if(get_field("youtube_id")=='' && get_field("vimeo_id")=='' && get_field("background_video")=='')
	{
	    echo '<img src="'.get_field("background_image").'" style="position:absolute; z-index:-2;">';
	}
	?>
<?php 
    $video_type = get_field("video_type");
    $video_url = "";
    if($video_type=="youtube"){
      $video_url = get_field("youtube_id");
	  if($video_url!='')
	  {
		?>
		   <iframe id="player" class="back-player-home" width="650" height="500" src="https://www.youtube.com/embed/<?php the_field('youtube_id');?>?autoplay=1" allowfullscreen></iframe>
		<?php 
	   }    
      }
    else if($video_type=="vimeo"){
      $video_url = get_field("vimeo_id");
	  if($video_url!='')
		{
		  ?>
			   <iframe id="player" class="back-player-home" src="//player.vimeo.com/video/<?php the_field('vimeo_id');?>?autoplay=1" width="650" height="500" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		 <?php
		}
      } 
    else {
      $video_url = get_field("background_video");
      if($video_url!='')
			{
		  ?>
		  	<video autoplay="" loop="" class="fillWidth fadeIn animated" poster="<?php the_field('background_image');?>" id="video-background">
    		<source src="<?php the_field('background_video');?>" type="video/mp4">Your browser does not support the video tag. I suggest you upgrade your browser.
				</video>
		 <?php
		}
    } 
    ?>
<div class="home2">
<div class="center-vertical-container">
<div class="center-vertical-content home2-table-cell">
<div class="home2-wrapper">
<div class="container home2-container">
<h1 class="light text-center text-shadow text-uppercase"><?php the_field("big_title");?></h1>
<h4 class="light text-center text-shadow"><?php the_field("sub_title");?></h4>  
<div style="float: left;width: 100%;position: relative;">
<?php if(!get_field("disable_services")):?>
        
<!-- start: desktop -->
<div class="home-services hidden-sm hidden-xs" data-navigation="nav-service-home2" style="max-height:463px;">
<div class="crsl-wrap">
            <?php
            $args = array(
              'post_type' => 'service_post' ,
              'nopaging' => true
            );             
            $the_Query = new WP_Query($args);
          
            while ($the_Query->have_posts()):
            $the_Query->the_post(); ?>
                <div class="col-md-4 crsl-item">
                  <div class="home-service">
                    <div class="service-photo">
                      <?php if(get_field('background_image',$post->ID)) {               
              $image = wp_get_attachment_image_src( get_field('background_image',$post->ID), 'authors' );
                echo '<img src="'.$image[0].'" alt="service-photo" />';
              }           
              ?>
               <div class="service-photo-pattern" style="background: <?php the_field('font_awesome_color'); ?>;"></div>
                      <div class="icon">
              <?php if(get_field('service_type')=='fontawsome'): ?>
                <i class="fa <?php the_field('font_awesome'); ?> fa-3x"></i>
              <?php else : ?>
                <img src="<?php the_field('image'); ?>" alt="service-photo" />
              <?php endif; ?>
              </div>
                    </div>
                    <div class="service-info">    
                      <h3><?php the_title();?></h3>
                      <?php the_content();?>
                   </div>
                  </div>
                </div>
            <?php endwhile; ?>     
</div><!-- ./crsl-wrap -->
</div><!-- ./home-services -->
<!-- end: desktop code -->   
<!-- start: mobile code -->
<div class="hidden-lg hidden-md col-sm-12 col-xs-12">
<?php
	$argsmobile = array(
	'post_type' => 'service_post',
	'nopaging' => false,
	'posts_per_page' => '3',
	'order' => 'DESC',
	'orderby'=> 'DATE',
	);             
	$the_Querymobile = new WP_Query($argsmobile);
          
	while ($the_Querymobile->have_posts()):
	$the_Querymobile->the_post(); ?>
	
<div class="home-service">

<div class="service-info">
<div class="service-photo">
<div class="icon">
<?php if(get_field('service_type')=='fontawsome'): ?>
<i class="fa <?php the_field('font_awesome'); ?> fa-3x" style="color: <?php the_field('font_awesome_color'); ?>;"></i>
<?php else : ?>
<img src="<?php the_field('image'); ?>" alt="service-photo" />
<?php endif; ?>
</div>
</div><!-- service-photo -->
<div>
<h3><?php the_title();?></h3>
</div>
</div>
</div><!-- ./home-service -->

<?php endwhile; ?> 

</div><!-- ./col-sm-12 hidden-lg hidden-md -->  
<!-- end mobile code -->       

<div id="nav-service-home2" class="crsl-nav hidden-sm hidden-xs">
<a href="#" class="previous"><i class="fa fa-chevron-left"></i></a>
<a href="#" class="next"><i class="fa fa-chevron-right"></i></a>
</div>
<?php endif; ?>
<?php  wp_reset_postdata();?>
</div><!-- style -->
</div><!-- ./container home2-container -->
</div><!-- ./home2-wrapper -->
</div><!-- ./center-vertical-content home2-table-cell -->
</div><!-- ./center-vertical-container -->
</div><!-- ./home2 -->
</div><!-- ./slides -->

<script>
   var BV;
   var background_image = "<?php the_field("background_image");?>";	
   var background_video = "<?php the_field("background_video");?>";	
   var video_type = "youtube"; // hack: leave as youtube regardless of video_type
    jQuery(document).ready(function($){	
	   home2();	 
     $('#slides').superslides({
      animation: 'fade',
      pagination: false
    });
     if($('#player').length != 0){
      centerVideo();
     }
    });
   
</script>

<!-- new code added to show copy -->

<main>
<div class="container">
<div class="homecopy col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
<?php echo the_field('homecopy_field'); ?>
</div><!-- ./homecopy -->
</div><!-- ./container -->
</main>
</div>
<!-- end: template-home2.php -->
<?php get_footer();?>