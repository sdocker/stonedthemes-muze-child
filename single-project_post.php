<!-- Pectopah changes: add banner to page; remove full screen expand; put slider in container -->
<?php get_header();?>
<!-- banner -->
<div id="title-container" class="full-background-image" style="background: url('http://pectopah.com/wp-content/uploads/2014/03/st-johns_1000_road-crop.jpg');">
<div id="linearBg" class="title-container-pattern" style="background-color: #375e97;background-repeat: repeat-y;background: -webkit-gradient(linear, left top, right top, from(#375e97), to(#697b96));background: -webkit-linear-gradient(left, #375e97, #697b96);background: -moz-linear-gradient(left, #375e97, #697b96);background: -ms-linear-gradient(left, #375e97, #697b96);background: -o-linear-gradient(left, #375e97, #697b96);opacity: 0.7;">
</div>
<div class="title-info">
		<h1 class="light"><?php the_title();?></h1>
		<h4 class="light"><?php the_field("header_subtitle");?></h4>
</div>
</div><!-- ./full-background-image -->
<!-- ./banner -->

<div class="single-port" style="height: 100%">

<div id="fb-root"></div>

<div class="container">

<!-- START: Yoast breadcrumb -->
<?php
if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('
<p id="breadcrumbs">','</p>
');
}
?>
<!-- END: Yoast breadcrumb -->
<div class="col-xs-12 col-sm-12 col-md-4 col-md-push-8">
<h3 class="light tight"><?php the_title();?></h3>
<?php  setup_postdata($post);?>
<?php  the_content();?>
<?php  wp_reset_postdata();?>
</div><!-- ./col-md-4 -->
<div class="col-xs-12 col-sm-12 col-md-8 col-md-pull-4">
<!-- Slider start -->
<?php if(get_field('type')=='slider'){?>
<div class="slides-container">
<?php $gallery = get_field('gallery');?>
<div id="blog-carousel" class="carousel slide" data-ride="carousel">
<div class="carousel-inner">
<?php $gallery_counter = 1;?>
<?php foreach($gallery as $g):?>
<div class="item <?php if($gallery_counter==1){ echo 'active'; } ?>">
<img src="<?php echo $g['image'];?>" width="1024" height="682" alt="Surly">
</div>
<?php $gallery_counter++; endforeach; ?>
</div>
<a class="lightbox-carousel-nav prev" href="#blog-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
<a class="lightbox-carousel-nav next" href="#blog-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
</div>
<?php } ?>
	<!-- Slider end -->
	

</div>
</div><!-- ../col-md-8 -->



</div><!-- ./container -->
<script>
	  jQuery(document).ready(function($){
	    jQuery('.carousel').carousel();
		shortcodes();
	  });
</script>
<?php get_footer();?>